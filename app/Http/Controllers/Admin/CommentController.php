<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class CommentController extends Controller
{
   public function index(){
       $comments = Comment::latest()->get();
       return view('backend.comment.index',compact('comments'));
   }
   public function destroy(Comment $comment){
       $comment->delete();
       Toastr::success('Comment Successfully Deleted :)', 'Success');
       return redirect()->back();
   }
}
