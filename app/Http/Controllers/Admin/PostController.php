<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Notifications\AuthorPostApprove;
use App\Notifications\NewPostNotify;
use App\post;
use App\Subscriber;
use App\Tag;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->get();
        return view('backend.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('backend.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'status' => 'required',
            'image' => 'required',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required'
        ]);
        $posts = new Post();
        $posts->user_id = Auth::id();
        $posts->title = $request->title;
        $posts->slug = Str::slug($request->title);
        //image section start
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/', $fileName);
            $posts->image = $fileName;
        } else {
            $posts->image = "default.png";
        }
        //image section end

        if (isset($request->status)) {
            $posts->status = true;
        } else {
            $posts->status = false;
        }
        $posts->body = $request->body;
        $posts->is_approved = true;
        $posts->save();
        $posts->categories()->attach($request->categories);
        $posts->tags()->attach($request->tags);
        //notify subscribers when a post create
        $subscribers = Subscriber::all();
        foreach ($subscribers as $subscriber){
            Notification::route('mail' , $subscriber->email)->notify(new NewPostNotify($posts));
        }

        Toastr::success('Post created successfully', 'Success');
        return redirect()->route('admin.post.index');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function show(post $post)
    {
        return view('backend.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('backend.post.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'required',
            'status' => 'required',
            'image' => 'required',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required'
        ]);
        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->slug = Str::slug($request->title);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/', $fileName);
            $post->image = $fileName;
        }
        if (isset($request->status)) {
            $post->status = true;
        } else {
            $post->status = false;
        }

        $post->body = $request->body;
        $post->is_approved = true;
        $post->save();
        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);
        Toastr::success('Post Updated successfully', 'Success');
        return redirect()->route('admin.post.index');
    }

    public function pending()
    {
        $posts = Post::where('is_approved', false)->get();
        return view('backend.post.pending', compact('posts'));
    }

    public function approve($id)
    {
        $posts = Post::find($id);
        if ($posts->is_approved == false) {
            $posts->is_approved = true;
            $posts->save();
            $posts->user->notify(new AuthorPostApprove($posts));
            Toastr::success('Post is successfully approved', 'Success');
        } else {
            Toastr::info('Post is already approved', 'Success');
        }
        return redirect()->route('admin.post.pending');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->categories()->detach();
        $post->tags()->detach();
        $post->delete();
        Toastr::success('Post Successfully Deleted :)', 'Success');
        return redirect()->back();
    }
}
