<?php

namespace App\Http\Controllers;


use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavouriteController extends Controller
{
    public function add($post){
        $user = Auth::user();
        $isFavourite = $user->fav_posts()->where('post_id',$post)->count();

        if ($isFavourite == 0 ){
            $user->fav_posts()->attach($post);
            Toastr::success('Post successfully added to the favourite list ','Success');
            return redirect()->back();
        }else{
            $user->fav_posts()->detach($post);
            Toastr::success('Post successfully removed to the favourite list ','Success');
            return redirect()->back();
        }
    }
}
