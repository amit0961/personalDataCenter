<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class PostController extends Controller
{
    public function details($slug){
        $post = Post::where('slug',$slug)->approved()->published()->first();
        //forViewCount on the post by reloading -> .env &session using lifetime **
        $blogkey = 'blog_'.$post->id ;
        if (!Session::has($blogkey)){
            $post->increment('view_count');
            Session::put($blogkey,1);
        }

        $randomposts = Post::approved()->published()->take(3)->inRandomOrder()->get();
        return view('frontend.details',compact('post','randomposts'));
    }

    public function allPost(){
        $posts = Post::latest()->approved()->published()->paginate(6);
//        return $posts ;
        return view('frontend.posts',compact('posts'));
    }
    public function postByCategory($slug){
        $category = Category::where('slug',$slug)->first();
        $posts = $category->posts()->approved()->published()->get();
        return view('frontend.postByCategory',compact('category','posts'));
    }
    public function postByTag($slug){
        $tag = Tag::where('slug',$slug)->first();
        $posts = $tag->posts()->approved()->published()->get();
        return view('frontend.postByTag',compact('tag','posts'));
    }



}
