@extends('layouts.backend.backendDesign')
@section('title' , 'Tag')
@push('css')

@endpush
@section('content')
    <section class="content">
        <div class="container-fluid">

            <div class="block-header">
                <a class="btn btn-primary waves-effect" href="{{ route('admin.tag.index') }}">
                    <i class="material-icons">login</i>
                    <span>TAG-LIST</span>
                </a>
            </div>
            <!-- Vertical Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                    <div class="card">
                        <div class="header">
                            <h2>
                                UPDATE TAG
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{route('admin.tag.update', $tag->id)}}" method="POST">
                                {{csrf_field()}}
                                {{ method_field('PUT') }} <!-- this is for resources controller when work in UpDate -->
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="name" id="name" class="form-control" value="{{$tag->name}}">
                                        <label class="form-label" for="name">Tag Name</label>
                                    </div>
                                </div>
                                <br>
                                <a href="{{route('admin.tag.index')}}" class="btn btn-danger m-t-15 waves-effect">BACK</a>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">UPDATE</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Vertical Layout | With Floating Label -->
        </div>
    </section>

    @push('js')

    @endpush

@stop
