@extends('layouts.frontend.frontendDesign')
@section('title')
{{$post->title}}
@endsection
@push('css')
    <link href="{{asset('assets/frontend/single-post-1/css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/common-css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/single-post-1/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/common-css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/layout-1/css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/layout-1/css/responsive.css')}}" rel="stylesheet">
    <style>
        .header-bg{
            height: 450px;
            width: 100%;
            background-image: url({{asset('storage/'.$post->image)}});
            background-size: cover;

        }
    </style>

@endpush
@section('content')
    <div class="header-bg shadow">

    </div><!-- slider -->

    <section class="post-area section">
        <div class="container">

            <div class="row">

                <div class="col-lg-8 col-md-12 no-right-padding">

                    <div class="main-post">

                        <div class="blog-post-inner">

                            <div class="post-info">

                                <div class="left-area">
                                    <a class="avatar" href="#"><img src="{{asset('storage/profile/'.$post->user->image)}}" alt="Profile Image"></a>
                                </div>

                                <div class="middle-area">
                                    <a class="name" href="#"><b>{{$post->user->name}}</b></a>
                                    <h6 class="date">{{$post->created_at->diffForHumans()}}</h6>
                                </div>

                            </div><!-- post-info -->

                            <h3 class="title"><a href="#"><b>{{$post->title}}</b></a></h3>

                            <div class="para">
                                {!! html_entity_decode($post->body) !!}
                            </div>
                            <h4 class="title"><b>Tag:</b></h4>
                            <ul class="tags">
                                @foreach($post->tags as $tag)
                                    <li><a href="{{route('tag.posts', $tag->slug)}}">{{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </div><!-- blog-post-inner -->

                        <div class="post-icons-area">
                            <ul class="post-icons">
                                <li>
                                    @guest()
                                        <a href="javascript:void(0);" onclick="toastr.info('To add favorite list - You need to login first.','Info',{
                                                    closeButton: true,
                                                    progressBar: true
                                                })"><i class="ion-heart"></i>
                                            {{$post->fav_to_users->count()}}
                                        </a>
                                    @else
                                        <a href="javascript:void(0);" onclick="document.getElementById('favourite-form-{{$post->id}}').submit();" class="{{!Auth::user()->fav_posts->where('pivot.post_id',$post->id)->count()==0 ? 'favPost' : '' }}" ><i class="ion-heart"></i>
                                            {{$post->fav_to_users->count()}}
                                            <form id="favourite-form-{{$post->id}}" action="{{route('favourite.post', $post->id)}}" style="display: none" method="POST">
                                                @csrf
                                            </form>
                                        </a>
                                    @endguest()
                                </li>
                                <li><a href="#"><i class="ion-chatbubble"></i>6</a></li>
                                <li><a href="#"><i class="ion-eye"></i>{{$post->view_count}}</a></li>
                            </ul>

                            <ul class="icons">
                                <li>SHARE : </li>
                                <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="ion-social-pinterest"></i></a></li>
                            </ul>
                        </div>

                        <div class="post-footer post-info">

                            <div class="left-area">
                                <a class="avatar" href="#"><img src="{{asset('storage/profile/'.$post->user->image)}}" alt="Profile Image"></a>
                            </div>

                            <div class="middle-area">
                                <a class="name" href="#"><b>{{$post->user->name}}</b></a>
                                <h6 class="date">on {{\Carbon\Carbon::parse($post->created_at)->format('dM,Y')}} at {{\Carbon\Carbon::parse($post->created_at)->format(' H:i a')}}</h6>
                            </div>

                        </div><!-- post-info -->


                    </div><!-- main-post -->
                </div><!-- col-lg-8 col-md-12 -->

                <div class="col-lg-4 col-md-12 no-left-padding">

                    <div class="single-post info-area">

                        <div class="sidebar-area about-area">
                            <h4 class="title"><b>ABOUT Author</b></h4>
                            <p>{{$post->user->about}}</p>
                        </div>


                        <div class="tag-area">

                            <h4 class="title"><b>CATEGORIES:-</b></h4>
                            <ul>
                                @foreach($post->categories as $category)
                                    <li><a href="{{route('category.posts',$category->slug)}}">{{$category->name}}</a></li>
                                @endforeach
                            </ul>

                        </div><!-- subscribe-area -->

                    </div><!-- info-area -->

                </div><!-- col-lg-4 col-md-12 -->

            </div><!-- row -->

        </div><!-- container -->
    </section><!-- post-area -->

    <section class="recomended-area section">
        <div class="container">
            <div class="row">
                @foreach($randomposts as $random)
                    <div class="col-lg-4 col-md-6">
                        <div class="card h-100">
                            <div class="single-post post-style-1">
                                <div class="blog-image"><img src="{{asset('storage/'.$random->image)}}" alt="Blog Image"></div>

                                <a class="avatar" href="#"><img src="{{asset('storage/profile/'.$random->user->image)}}" alt="Profile Image"></a>

                                <div class="blog-info">

                                    <h4 class="title"><a href="{{route('post.details',$random->slug)}}"><b>{{$random->title}}</b></a></h4>

                                    <ul class="post-footer">
                                        <li>
                                            @guest()
                                                <a href="javascript:void(0);" onclick="toastr.info('To add favorite list - You need to login first.','Info',{
                                                    closeButton: true,
                                                    progressBar: true
                                                })"><i class="ion-heart"></i>
                                                    {{$random->fav_to_users->count()}}
                                                </a>
                                            @else
                                                <a href="javascript:void(0);" onclick="document.getElementById('favourite-form-{{$random->id}}').submit();" class="{{!Auth::user()->fav_posts->where('pivot.post_id',$post->id)->count()==0 ? 'favPost' : '' }}" ><i class="ion-heart"></i>
                                                    {{$random->fav_to_users->count()}}
                                                    <form id="favourite-form-{{$random->id}}" action="{{route('favourite.post', $random->id)}}" style="display: none" method="POST">
                                                        @csrf
                                                    </form>
                                                </a>
                                            @endguest()
                                        </li>
                                        <li><a href="#"><i class="ion-chatbubble"></i>{{$random->comments->count()}}</a></li>
                                        <li><a href="#"><i class="ion-eye"></i>{{$random->view_count}}</a></li>
                                    </ul>

                                </div><!-- blog-info -->
                            </div><!-- single-post -->
                        </div><!-- card -->
                    </div><!-- col-md-6 col-sm-12 -->
                @endforeach


            </div><!-- row -->

        </div><!-- container -->
    </section>

    <section class="comment-section">
        <div class="container">
            <h4><b>POST COMMENT</b></h4>
            <div class="row">

                <div class="col-lg-8 col-md-12">
                    <div class="comment-form">
                        @guest()
                            <p>For post a comment, you need to login first .
                                <a href="{{route('login')}}">Login</a></p>
                        @else
                            <form method="POST" action="{{route('comment.store', $post->id)}}">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-sm-12">
									<textarea name="comment" rows="2" class="text-area-messge form-control"
                                              placeholder="Enter your comment" aria-required="true" aria-invalid="false"></textarea >
                                    </div><!-- col-sm-12 -->
                                    <div class="col-sm-12">
                                        <button class="submit-btn" name="submit" type="submit" id="form-submit"><b>POST COMMENT</b></button>
                                    </div><!-- col-sm-12 -->

                                </div><!-- row -->
                            </form>
                        @endguest

                    </div><!-- comment-form -->

                    <h4><b>COMMENTS({{$post->comments()->count()}})</b></h4>
                    @if($post->comments->count() > 0)
                        @foreach($post->comments as $comment)
                            <div class="commnets-area ">

                                <div class="comment">

                                    <div class="post-info">

                                        <div class="left-area">
                                            <a class="avatar" href="#"><img src="{{asset('storage/profile/'.$comment->user->image)}}" alt="Profile Image"></a>
                                        </div>

                                        <div class="middle-area">
                                            <a class="name" href="#"><b>{{$comment->user->name}}</b></a>
                                            <h6 class="date">on {{$comment->created_at->diffForHumans()}}</h6>
                                        </div>

                                    </div><!-- post-info -->

                                    <p>{{$comment->comment}}</p>

                                </div>

                            </div><!-- commnets-area -->
                        @endforeach
                    @else
                        <div class="commnets-area ">

                            <div class="comment">
                                <p>No comments post yet.</p>
                            </div>
                        </div>
                    @endif


                    <a class="more-comment-btn" href="#"><b>VIEW MORE COMMENTS </b> </a>

                </div><!-- col-lg-8 col-md-12 -->

            </div><!-- row -->

        </div><!-- container -->
    </section>

@stop
@push('js')
    <script src="{{asset('assets/frontend/common-js/jquery-3.1.1.min.js')}}"></script>

    <script src="{{asset('assets/frontend/common-js/tether.min.js')}}"></script>

    <script src="{{asset('assets/frontend/common-js/bootstrap.js')}}"></script>

    <script src="{{asset('assets/frontend/common-js/scripts.js')}}"></script>
@endpush
