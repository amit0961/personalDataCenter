@extends('layouts.frontend.frontendDesign')
@section('title', 'Home')
@push('css')
    <link href="{{asset('assets/frontend/common-css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/common-css/swiper.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/common-css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/front-page-category/css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/front-page-category/css/responsive.css')}}" rel="stylesheet">




@endpush
@section('content')
    <div class="main-slider">
        <div class="swiper-container position-static" data-slide-effect="slide" data-autoheight="false"
             data-swiper-speed="1000" data-swiper-autoplay="10000" data-swiper-margin="0"
             data-swiper-slides-per-view="3"
             data-swiper-breakpoints="true" data-swiper-loop="true">
            <div class="swiper-wrapper">
                @foreach($categories as $category)
                    <div class="swiper-slide">
                        <a class="slider-category" href="{{route('category.posts', $category->slug)}}">
                            <div class="blog-image">
                                <img src="{{ asset('storage/category/slider/'.$category->image) }}"
                                     alt="{{$category->name}}">
                            </div>

                            <div class="category">
                                <div class="display-table center-text">
                                    <div class="display-table-cell">
                                        <h3><b>{{$category->name}}</b></h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div><!-- swiper-slide -->
                @endforeach


            </div><!-- swiper-wrapper -->

        </div><!-- swiper-container -->

    </div><!-- slider -->

    <section class="blog-area section">
        <div class="container">

            <div class="row">
                @foreach($posts as $post)
                    <div class="col-lg-4 col-md-6">
                        <div class="card h-100">
                            <div class="single-post post-style-1">

                                <div class="blog-image"><img
                                        src="{{asset('storage/'.$post->image)}}"
                                        alt="No Image"></div>

                                <a class="avatar" href="#"><img
                                        src="{{asset('storage/profile/'.$post->user->image)}}"
                                        alt="Profile Image"></a>

                                <div class="blog-info">

                                    <h4 class="title"><a href="{{route('post.details',$post->slug)}}"><b>{{$post->title}}</b></a></h4>

                                    <ul class="post-footer">
                                        <li>
                                            @guest()
                                                <a href="javascript:void(0);" onclick="toastr.info('To add favorite list - You need to login first.','Info',{
                                                    closeButton: true,
                                                    progressBar: true
                                                })"><i class="ion-heart"></i>
                                                    {{$post->fav_to_users->count()}}
                                                </a>
                                            @else
                                                <a href="javascript:void(0);" onclick="document.getElementById('favourite-form-{{$post->id}}').submit();" class="{{!Auth::user()->fav_posts->where('pivot.post_id',$post->id)->count()==0 ? 'favPost' : '' }}" ><i class="ion-heart"></i>
                                                    {{$post->fav_to_users->count()}}
                                                    <form id="favourite-form-{{$post->id}}" action="{{route('favourite.post', $post->id)}}" style="display: none" method="POST">
                                                        @csrf
                                                    </form>
                                                </a>
                                            @endguest()
                                        </li>

                                        <li><a href="#"><i class="ion-chatbubble"></i>{{$post->comments->count()}}</a></li>
                                        <li><a href="#"><i class="ion-eye"></i>{{$post->view_count}}</a></li>
                                    </ul>

                                </div><!-- blog-info -->
                            </div><!-- single-post -->
                        </div><!-- card -->
                    </div><!-- col-lg-4 col-md-6 -->

                @endforeach

            </div><!-- row -->

{{--            <a class="load-more-btn" href="#"><b>LOAD MORE</b></a>--}}
            {{$posts->links()}}

        </div><!-- container -->
    </section><!-- section -->

@stop
@push('js')
    <script src="{{asset('assets/frontend/common-js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('assets/frontend/common-js/tether.min.js')}}"></script>
    <script src="{{asset('assets/frontend/common-js/bootstrap.js')}}"></script>
    <script src="{{asset('assets/frontend/common-js/swiper.js')}}"></script>
    <script src="{{asset('assets/frontend/common-js/scripts.js')}}"></script>
@endpush
