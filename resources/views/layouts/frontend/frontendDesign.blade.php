<!DOCTYPE HTML>
<html lang="en">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')-{{ config('app.name', 'Laravel') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">


    <!-- Font -->

    <link href=" https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <style>
        .favPost{
            color: darkblue;
        }
    </style>

@stack('css')


</head>
<body >

@include('layouts.frontend.frontendHeader')
@yield('content')

@include('layouts.frontend.frontendFooter')

@stack('js')
<script src=" http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
<script src=" http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
{!! Toastr::message() !!}
<script>
    @if($errors->any())
    @foreach($errors->all() as $error)
    toastr.error('{{ $error }}', 'Error', {
        closeButton: true,
        progressBar: true,
    });
    @endforeach
    @endif
</script>
<script src="{{asset('assets/frontend/common-js/jquery-3.1.1.min.js')}}"></script>
</body>
</html>
