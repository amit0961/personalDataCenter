<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Route::get('/admin', function () {
//    return view('backend.dashboard');
//});



//for Frontend Section
Route::get('/', 'HomeController@index')->name('frontend.home');
Route::get('post/{slug}', 'PostController@details')->name('post.details');
Route::get('/posts', 'PostController@allPost')->name('post.allpost');
Route::get('/category/{slug}', 'PostController@postByCategory')->name('category.posts');
Route::get('/tag/{slug}', 'PostController@postByTag')->name('tag.posts');

Route::post('/subscriber', 'SubscriberController@store')->name('subscriber.store');

//Authorization Section
Auth::routes();

//Post Favroutie Section
Route::group(['middleware'=>['auth']], function (){
    Route::post('favourite/{post}/add', 'FavouriteController@add')->name('favourite.post');
    Route::post('comment/{post}' ,'CommentController@store')->name('comment.store');
});


//prefix acts like ( admin.dashboard in the url) & namespace acts folder path &&&& [as] use here like admin.index for route name section

Route::group([ 'as'=>'admin.', 'prefix'=>'admin', 'namespace'=>'Admin' , 'middleware'=>['auth','admin']] , function (){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('/tag', 'TagController');
    Route::resource('/category', 'CategoryController');
    Route::resource('/post', 'PostController');
    Route::get('/favourite' ,'FavouriteController@index')->name('favourite.index');

    Route::get('/comment' ,'CommentController@index')->name('comment.index');
    Route::delete('/comment/{comment}' ,'CommentController@destroy')->name('comment.destroy');

    Route::get('/subscriber' ,'SubscriberController@index')->name('subscriber.index');
    Route::delete('/subscriber/{subscriber}' ,'SubscriberController@destroy')->name('subscriber.destroy');
    Route::get('/settings' ,'SettingsController@index')->name('settings');
    Route::put('/settings/profile' ,'SettingsController@updateProfile')->name('settings.profile');
    Route::put('/settings/password' ,'SettingsController@updatePassword')->name('settings.password');

    Route::put('/post/{id}/approve', 'PostController@approve')->name('post.approve');
    Route::get('/pending/post','PostController@pending')->name('post.pending');


});

Route::group([ 'as'=>'author.', 'prefix'=>'author', 'namespace'=>'Author' , 'middleware'=>['auth','author']] , function (){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('/post', 'PostController');
    Route::get('/favourite' ,'FavouriteController@index')->name('favourite.index');
    Route::get('/comment' ,'CommentController@index')->name('comment.index');
    Route::delete('/comment/{comment}' ,'CommentController@destroy')->name('comment.destroy');
    Route::get('/settings' ,'SettingsController@index')->name('settings');
    Route::put('/settings/profile' ,'SettingsController@updateProfile')->name('settings.profile');
    Route::put('/settings/password' ,'SettingsController@updatePassword')->name('settings.password');
});

